-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

-- Menu State
-- Look at inventory, player stats

require "player/player"
require "data/itemData"
require "controllers/graphicController"
require "data/colorData"

stateMenu = {
	name = "Menu"
}

view = "inventory"

function stateMenu:Load()
	fntSmall = love.graphics.newFont( 9 )
	fntHeader = love.graphics.newFont( 12 )
end

function stateMenu:Update( deltaTime )
	retval = self:HandleInput()
	return retval -- Helps the controller know to change state
end

function stateMenu:Draw()
	self:DrawBase()
	if ( view == "inventory" ) then
		self:DrawInventory()
	elseif ( view == "stats" ) then
		self:DrawStats()
	end
end

-- **********************************
-- ** Additional functionality
-- **********************************

function stateMenu:DrawBase()
	gfx:DrawRectangle( "fill", { x = 0, y = 0 }, { w = 200, h = 200 }, colorKey.black )

	gfx:DrawText( "INVENTORY", { x = 25, y = 5 }, fntHeader, colorKey.white )
	gfx:DrawText( "STATS", { x = 125, y = 5 }, fntHeader, colorKey.white )

	gfx:DrawText( "Press [S] to toggle Sound!", { x = 40, y = 165 }, fntSmall, colorKey.white )
	gfx:DrawText( "Press [LEFT] and [RIGHT]", { x = 40, y = 175 }, fntSmall, colorKey.white )
	gfx:DrawText( "To scroll through menu", { x = 40, y = 185 }, fntSmall, colorKey.white )
end

function stateMenu:DrawInventory()
	gfx:DrawLine( { x = 25, y = 20 }, { x = 28+(9*7), y = 20 }, colorKey.white )
	
	outX = 5
	outX2 = 90
	outY = 30
	self:OutputItems( items.rarity1 )
	self:OutputItems( items.rarity2 )
	self:OutputItems( items.rarity3 )
end

function stateMenu:OutputItems( rarityCategory )
	love.graphics.setFont( fntSmall )
	for key, itemVal in pairs( rarityCategory ) do
		if ( key ~= "total" ) then
			local color
            if ( itemVal.count == 0 ) then
				color = colorKey.darkGrey
            elseif ( itemVal.count < 10 ) then
				color = colorKey.white
            else
				color = colorKey.orange
            end
			gfx:DrawText( itemVal.name .. ": ", 
				{ x = outX, y = outY }, fntSmall, color )
			gfx:DrawText( itemVal.count, 
				{ x = outX2, y = outY }, fntSmall, color )
				
			outX = outX + 100
			outX2 = outX2 + 100
			if ( outX >= 200 ) then
				outX = 5
				outX2 = 90
				outY = outY + 10
			end
		end
	end
end

function stateMenu:DrawStats()
	gfx:DrawLine( { x = 125, y = 20 }, { x = 128+(5*7), y = 20 }, colorKey.white )
	
	-- Name
	gfx:DrawText( "Cat: " .. player.catType, { x = 5, y = 30 }, fntSmall, colorKey.white )
	
	-- Stats
	gfx:DrawText( "HP: " .. math.floor(player.stats.HP), 
		{ x = 5, y = 50 }, fntSmall, colorKey.white )
	gfx:DrawText( "Attack: " .. player.stats.Atk, 
		{ x = 5, y = 60 }, fntSmall, colorKey.white )
	gfx:DrawText( "Defense: " .. player.stats.Def, 
		{ x = 5, y = 70 }, fntSmall, colorKey.white )
	
	-- Steps
	gfx:DrawText( "Steps taken: " .. math.floor(player.totalSteps), 
		{ x = 5, y = 90 }, fntSmall, colorKey.white )
	gfx:DrawText( "Rank: " .. player:GetRank(), 
		{ x = 5, y = 100 }, fntSmall, colorKey.white )
	gfx:DrawText( "Total kills: " .. player.totalKills, 
		{ x = 5, y = 110 }, fntSmall, colorKey.white )
end

function stateMenu:HandleInput()
	if ( love.keyboard.isDown( "left" ) ) then
		view = "inventory"
	elseif ( love.keyboard.isDown( "right" ) ) then
		view = "stats"
	end
	
	if ( keyboard:InputOK() and love.keyboard.isDown( "escape" ) ) then
        keyboard:FreezeInput()
		return "stateGame"
	end
	return ""
end
