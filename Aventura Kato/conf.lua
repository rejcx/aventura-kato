-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

function love.conf( t )
	t.screen.width = 640
	t.screen.height = 480
	--t.screen.width = 200
	--t.screen.height = 200
	t.title = "Aventuro Kato"
	t.author = "Rachel J. Morris"
	t.url = "http://www.moosader.com/"
end
