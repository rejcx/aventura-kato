-- Aventuro Kato - Adventure Cat!
-- www.moosader.com
-- (c) Rachel J. Morris, 2012
-- Licensed TBD

-- Need a keyboard cooldown to help with not "double-tapping"
-- a key to go through multiple menus at once

keyboard = {
	cooldown = 0,
	cooldownMax = 1000
}

function keyboard:InputOK()
	if ( self.cooldown > 0 ) then
		return false
	end
	return true
end

function keyboard:FreezeInput()
	self.cooldown = self.cooldownMax
end

function keyboard:Update( deltaTime )
	if ( self.cooldown > 0 ) then
		self.cooldown = self.cooldown - 1
	end
end